# Docker Build Mounts

https://datasciencedecoded.gitlab.io/blog/posts/2023/11/docker-build-mounts/

|             | Image Size | Docker Build Time | Docker (re)Build Time |
|-------------|------------|-------------------|-----------------------|
| baseline    |   8.08GB   |      160s         |       160s            |
| cache-purge |   5.32GB   |      160s         |       160s            |
| no-cache    |   5.32GB   |      160s         |       160s            |
| build-mount | __5.32GB__ |      160s         |     __110s__          |

To build an image:

```bash
make NAME=<name>
```

There are four named build strategies to compare: `baseline`, `cache-purge`, `no-cache`, `build-mount`

To update Python dependencies in `requirements.lock`, update `requirements.txt` as desired, then:

```bash
make pip-lock
```