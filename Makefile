NAME ?= mount

default: build

pip-lock:
	@docker build \
		-t lock \
		-f docker/lock/Dockerfile \
		.
	@docker run -it --rm \
		lock > requirements.lock
	

build:
	@docker build \
		-t $(NAME) \
		-f docker/$(NAME)/Dockerfile \
		.
